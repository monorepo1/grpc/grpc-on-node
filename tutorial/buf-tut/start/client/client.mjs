import messages from '../gen/proto/js/pet/v1/pet_pb.js'
import services from '../gen/proto/js/pet/v1/pet_grpc_pb.js'
import { credentials } from '@grpc/grpc-js'

const target = 'localhost:50051'
const client = new services.PetStoreServiceClient(target, credentials.createInsecure())
const request = new messages.PutPetRequest()
request.setPetType(messages.PetType.PET_TYPE_SNAKE)
request.setName('Mike')

client.putPet(request, (err, response) => {
    if (err) {
        console.error({ err })
    } else {
        console.info({ err, request, response, pet: response.getPet().toObject() })
    }
})