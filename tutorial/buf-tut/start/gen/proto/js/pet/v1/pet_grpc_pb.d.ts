// package: pet.v1
// file: pet/v1/pet.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as pet_v1_pet_pb from "../../pet/v1/pet_pb";
import * as google_type_datetime_pb from "../../google/type/datetime_pb";

interface IPetStoreServiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    getPet: IPetStoreServiceService_IGetPet;
    putPet: IPetStoreServiceService_IPutPet;
    deletePet: IPetStoreServiceService_IDeletePet;
}

interface IPetStoreServiceService_IGetPet extends grpc.MethodDefinition<pet_v1_pet_pb.GetPetRequest, pet_v1_pet_pb.GetPetResponse> {
    path: "/pet.v1.PetStoreService/GetPet";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<pet_v1_pet_pb.GetPetRequest>;
    requestDeserialize: grpc.deserialize<pet_v1_pet_pb.GetPetRequest>;
    responseSerialize: grpc.serialize<pet_v1_pet_pb.GetPetResponse>;
    responseDeserialize: grpc.deserialize<pet_v1_pet_pb.GetPetResponse>;
}
interface IPetStoreServiceService_IPutPet extends grpc.MethodDefinition<pet_v1_pet_pb.PutPetRequest, pet_v1_pet_pb.PutPetResponse> {
    path: "/pet.v1.PetStoreService/PutPet";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<pet_v1_pet_pb.PutPetRequest>;
    requestDeserialize: grpc.deserialize<pet_v1_pet_pb.PutPetRequest>;
    responseSerialize: grpc.serialize<pet_v1_pet_pb.PutPetResponse>;
    responseDeserialize: grpc.deserialize<pet_v1_pet_pb.PutPetResponse>;
}
interface IPetStoreServiceService_IDeletePet extends grpc.MethodDefinition<pet_v1_pet_pb.DeletePetRequest, pet_v1_pet_pb.DeletePetResponse> {
    path: "/pet.v1.PetStoreService/DeletePet";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<pet_v1_pet_pb.DeletePetRequest>;
    requestDeserialize: grpc.deserialize<pet_v1_pet_pb.DeletePetRequest>;
    responseSerialize: grpc.serialize<pet_v1_pet_pb.DeletePetResponse>;
    responseDeserialize: grpc.deserialize<pet_v1_pet_pb.DeletePetResponse>;
}

export const PetStoreServiceService: IPetStoreServiceService;

export interface IPetStoreServiceServer extends grpc.UntypedServiceImplementation {
    getPet: grpc.handleUnaryCall<pet_v1_pet_pb.GetPetRequest, pet_v1_pet_pb.GetPetResponse>;
    putPet: grpc.handleUnaryCall<pet_v1_pet_pb.PutPetRequest, pet_v1_pet_pb.PutPetResponse>;
    deletePet: grpc.handleUnaryCall<pet_v1_pet_pb.DeletePetRequest, pet_v1_pet_pb.DeletePetResponse>;
}

export interface IPetStoreServiceClient {
    getPet(request: pet_v1_pet_pb.GetPetRequest, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.GetPetResponse) => void): grpc.ClientUnaryCall;
    getPet(request: pet_v1_pet_pb.GetPetRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.GetPetResponse) => void): grpc.ClientUnaryCall;
    getPet(request: pet_v1_pet_pb.GetPetRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.GetPetResponse) => void): grpc.ClientUnaryCall;
    putPet(request: pet_v1_pet_pb.PutPetRequest, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.PutPetResponse) => void): grpc.ClientUnaryCall;
    putPet(request: pet_v1_pet_pb.PutPetRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.PutPetResponse) => void): grpc.ClientUnaryCall;
    putPet(request: pet_v1_pet_pb.PutPetRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.PutPetResponse) => void): grpc.ClientUnaryCall;
    deletePet(request: pet_v1_pet_pb.DeletePetRequest, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.DeletePetResponse) => void): grpc.ClientUnaryCall;
    deletePet(request: pet_v1_pet_pb.DeletePetRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.DeletePetResponse) => void): grpc.ClientUnaryCall;
    deletePet(request: pet_v1_pet_pb.DeletePetRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.DeletePetResponse) => void): grpc.ClientUnaryCall;
}

export class PetStoreServiceClient extends grpc.Client implements IPetStoreServiceClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public getPet(request: pet_v1_pet_pb.GetPetRequest, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.GetPetResponse) => void): grpc.ClientUnaryCall;
    public getPet(request: pet_v1_pet_pb.GetPetRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.GetPetResponse) => void): grpc.ClientUnaryCall;
    public getPet(request: pet_v1_pet_pb.GetPetRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.GetPetResponse) => void): grpc.ClientUnaryCall;
    public putPet(request: pet_v1_pet_pb.PutPetRequest, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.PutPetResponse) => void): grpc.ClientUnaryCall;
    public putPet(request: pet_v1_pet_pb.PutPetRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.PutPetResponse) => void): grpc.ClientUnaryCall;
    public putPet(request: pet_v1_pet_pb.PutPetRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.PutPetResponse) => void): grpc.ClientUnaryCall;
    public deletePet(request: pet_v1_pet_pb.DeletePetRequest, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.DeletePetResponse) => void): grpc.ClientUnaryCall;
    public deletePet(request: pet_v1_pet_pb.DeletePetRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.DeletePetResponse) => void): grpc.ClientUnaryCall;
    public deletePet(request: pet_v1_pet_pb.DeletePetRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: pet_v1_pet_pb.DeletePetResponse) => void): grpc.ClientUnaryCall;
}
