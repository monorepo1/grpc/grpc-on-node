// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var pet_v1_pet_pb = require('../../pet/v1/pet_pb.js');
var google_type_datetime_pb = require('../../google/type/datetime_pb.js');

function serialize_pet_v1_DeletePetRequest(arg) {
  if (!(arg instanceof pet_v1_pet_pb.DeletePetRequest)) {
    throw new Error('Expected argument of type pet.v1.DeletePetRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_pet_v1_DeletePetRequest(buffer_arg) {
  return pet_v1_pet_pb.DeletePetRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_pet_v1_DeletePetResponse(arg) {
  if (!(arg instanceof pet_v1_pet_pb.DeletePetResponse)) {
    throw new Error('Expected argument of type pet.v1.DeletePetResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_pet_v1_DeletePetResponse(buffer_arg) {
  return pet_v1_pet_pb.DeletePetResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_pet_v1_GetPetRequest(arg) {
  if (!(arg instanceof pet_v1_pet_pb.GetPetRequest)) {
    throw new Error('Expected argument of type pet.v1.GetPetRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_pet_v1_GetPetRequest(buffer_arg) {
  return pet_v1_pet_pb.GetPetRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_pet_v1_GetPetResponse(arg) {
  if (!(arg instanceof pet_v1_pet_pb.GetPetResponse)) {
    throw new Error('Expected argument of type pet.v1.GetPetResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_pet_v1_GetPetResponse(buffer_arg) {
  return pet_v1_pet_pb.GetPetResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_pet_v1_PutPetRequest(arg) {
  if (!(arg instanceof pet_v1_pet_pb.PutPetRequest)) {
    throw new Error('Expected argument of type pet.v1.PutPetRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_pet_v1_PutPetRequest(buffer_arg) {
  return pet_v1_pet_pb.PutPetRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_pet_v1_PutPetResponse(arg) {
  if (!(arg instanceof pet_v1_pet_pb.PutPetResponse)) {
    throw new Error('Expected argument of type pet.v1.PutPetResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_pet_v1_PutPetResponse(buffer_arg) {
  return pet_v1_pet_pb.PutPetResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var PetStoreServiceService = exports.PetStoreServiceService = {
  getPet: {
    path: '/pet.v1.PetStoreService/GetPet',
    requestStream: false,
    responseStream: false,
    requestType: pet_v1_pet_pb.GetPetRequest,
    responseType: pet_v1_pet_pb.GetPetResponse,
    requestSerialize: serialize_pet_v1_GetPetRequest,
    requestDeserialize: deserialize_pet_v1_GetPetRequest,
    responseSerialize: serialize_pet_v1_GetPetResponse,
    responseDeserialize: deserialize_pet_v1_GetPetResponse,
  },
  putPet: {
    path: '/pet.v1.PetStoreService/PutPet',
    requestStream: false,
    responseStream: false,
    requestType: pet_v1_pet_pb.PutPetRequest,
    responseType: pet_v1_pet_pb.PutPetResponse,
    requestSerialize: serialize_pet_v1_PutPetRequest,
    requestDeserialize: deserialize_pet_v1_PutPetRequest,
    responseSerialize: serialize_pet_v1_PutPetResponse,
    responseDeserialize: deserialize_pet_v1_PutPetResponse,
  },
  deletePet: {
    path: '/pet.v1.PetStoreService/DeletePet',
    requestStream: false,
    responseStream: false,
    requestType: pet_v1_pet_pb.DeletePetRequest,
    responseType: pet_v1_pet_pb.DeletePetResponse,
    requestSerialize: serialize_pet_v1_DeletePetRequest,
    requestDeserialize: deserialize_pet_v1_DeletePetRequest,
    responseSerialize: serialize_pet_v1_DeletePetResponse,
    responseDeserialize: deserialize_pet_v1_DeletePetResponse,
  },
};

exports.PetStoreServiceClient = grpc.makeGenericClientConstructor(PetStoreServiceService);
