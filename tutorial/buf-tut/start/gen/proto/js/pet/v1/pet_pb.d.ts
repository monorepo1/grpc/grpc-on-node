// package: pet.v1
// file: pet/v1/pet.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";
import * as google_type_datetime_pb from "../../google/type/datetime_pb";

export class Pet extends jspb.Message { 
    getPetType(): PetType;
    setPetType(value: PetType): Pet;
    getPetId(): string;
    setPetId(value: string): Pet;
    getName(): string;
    setName(value: string): Pet;

    hasCreatedAt(): boolean;
    clearCreatedAt(): void;
    getCreatedAt(): google_type_datetime_pb.DateTime | undefined;
    setCreatedAt(value?: google_type_datetime_pb.DateTime): Pet;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Pet.AsObject;
    static toObject(includeInstance: boolean, msg: Pet): Pet.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Pet, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Pet;
    static deserializeBinaryFromReader(message: Pet, reader: jspb.BinaryReader): Pet;
}

export namespace Pet {
    export type AsObject = {
        petType: PetType,
        petId: string,
        name: string,
        createdAt?: google_type_datetime_pb.DateTime.AsObject,
    }
}

export class GetPetRequest extends jspb.Message { 
    getPetId(): string;
    setPetId(value: string): GetPetRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetPetRequest.AsObject;
    static toObject(includeInstance: boolean, msg: GetPetRequest): GetPetRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetPetRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetPetRequest;
    static deserializeBinaryFromReader(message: GetPetRequest, reader: jspb.BinaryReader): GetPetRequest;
}

export namespace GetPetRequest {
    export type AsObject = {
        petId: string,
    }
}

export class GetPetResponse extends jspb.Message { 

    hasPet(): boolean;
    clearPet(): void;
    getPet(): Pet | undefined;
    setPet(value?: Pet): GetPetResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetPetResponse.AsObject;
    static toObject(includeInstance: boolean, msg: GetPetResponse): GetPetResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetPetResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetPetResponse;
    static deserializeBinaryFromReader(message: GetPetResponse, reader: jspb.BinaryReader): GetPetResponse;
}

export namespace GetPetResponse {
    export type AsObject = {
        pet?: Pet.AsObject,
    }
}

export class PutPetRequest extends jspb.Message { 
    getPetType(): PetType;
    setPetType(value: PetType): PutPetRequest;
    getName(): string;
    setName(value: string): PutPetRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PutPetRequest.AsObject;
    static toObject(includeInstance: boolean, msg: PutPetRequest): PutPetRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PutPetRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PutPetRequest;
    static deserializeBinaryFromReader(message: PutPetRequest, reader: jspb.BinaryReader): PutPetRequest;
}

export namespace PutPetRequest {
    export type AsObject = {
        petType: PetType,
        name: string,
    }
}

export class PutPetResponse extends jspb.Message { 

    hasPet(): boolean;
    clearPet(): void;
    getPet(): Pet | undefined;
    setPet(value?: Pet): PutPetResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PutPetResponse.AsObject;
    static toObject(includeInstance: boolean, msg: PutPetResponse): PutPetResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PutPetResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PutPetResponse;
    static deserializeBinaryFromReader(message: PutPetResponse, reader: jspb.BinaryReader): PutPetResponse;
}

export namespace PutPetResponse {
    export type AsObject = {
        pet?: Pet.AsObject,
    }
}

export class DeletePetRequest extends jspb.Message { 
    getPetId(): string;
    setPetId(value: string): DeletePetRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DeletePetRequest.AsObject;
    static toObject(includeInstance: boolean, msg: DeletePetRequest): DeletePetRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DeletePetRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DeletePetRequest;
    static deserializeBinaryFromReader(message: DeletePetRequest, reader: jspb.BinaryReader): DeletePetRequest;
}

export namespace DeletePetRequest {
    export type AsObject = {
        petId: string,
    }
}

export class DeletePetResponse extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DeletePetResponse.AsObject;
    static toObject(includeInstance: boolean, msg: DeletePetResponse): DeletePetResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DeletePetResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DeletePetResponse;
    static deserializeBinaryFromReader(message: DeletePetResponse, reader: jspb.BinaryReader): DeletePetResponse;
}

export namespace DeletePetResponse {
    export type AsObject = {
    }
}

export enum PetType {
    PET_TYPE_UNSPECIFIED = 0,
    PET_TYPE_CAT = 1,
    PET_TYPE_DOG = 2,
    PET_TYPE_SNAKE = 3,
    PET_TYPE_HAMSTER = 4,
}
