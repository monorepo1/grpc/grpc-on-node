import { Server, ServerCredentials } from "@grpc/grpc-js";
import messages from '../gen/proto/js/pet/v1/pet_pb.js'
import services from '../gen/proto/js/pet/v1/pet_grpc_pb.js'

function putPet (call, callback) {
    const message = `Got a request to create a ${call.request.getPetType()} named ${call.request.getName()}.`
    console.info({ putPet: message })
    const reply = new messages.PutPetResponse()
/**
 * proto.pet.v1.Pet.toObject = function(includeInstance, msg) {
  var f, obj = {
    petType: jspb.Message.getFieldWithDefault(msg, 1, 0),
    petId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    name: jspb.Message.getFieldWithDefault(msg, 3, ""),
    createdAt: (f = msg.getCreatedAt()) && google_type_datetime_pb.DateTime.toObject(includeInstance, f)
  };

  proto.pet.v1.Pet.prototype.setPetType = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};
proto.pet.v1.Pet.prototype.setPetId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};
proto.pet.v1.Pet.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};
proto.pet.v1.Pet.prototype.setCreatedAt = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};

proto.pet.v1.GetPetResponse.prototype.setPet = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};
 */
    const pet = new messages.Pet()
    pet.setPetType(call.request.getPetType())
      .setName(call.request.getName())
      .setPetId('1')
      // .setCreatedAt(Date.now())
    reply.setPet(pet)
    callback(null, reply)
}

const server = new Server()
server.addService(services.PetStoreServiceService, {
    putPet
})
server.bindAsync(
    '0.0.0.0:50051', 
    ServerCredentials.createInsecure(),
    () => {
        server.start()
        console.info('Server listening on port 50051')
    }
)
