// package: byhiras.datasource
// file: byhiras/datasource/service.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as byhiras_datasource_service_pb from "../../byhiras/datasource/service_pb";
import * as byhiras_datasource_datasource_pb from "../../byhiras/datasource/datasource_pb";
import * as byhiras_delta_json_patch_pb from "../../byhiras/delta/json_patch_pb";

interface IDataSourceQueryServiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    getVersion: IDataSourceQueryServiceService_IGetVersion;
    getById: IDataSourceQueryServiceService_IGetById;
    listByIds: IDataSourceQueryServiceService_IListByIds;
    list: IDataSourceQueryServiceService_IList;
    streamPage: IDataSourceQueryServiceService_IStreamPage;
    listContributors: IDataSourceQueryServiceService_IListContributors;
}

interface IDataSourceQueryServiceService_IGetVersion extends grpc.MethodDefinition<byhiras_datasource_datasource_pb.GetVersionRequest, byhiras_datasource_datasource_pb.GetVersionResponse> {
    path: "/byhiras.datasource.DataSourceQueryService/GetVersion";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<byhiras_datasource_datasource_pb.GetVersionRequest>;
    requestDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.GetVersionRequest>;
    responseSerialize: grpc.serialize<byhiras_datasource_datasource_pb.GetVersionResponse>;
    responseDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.GetVersionResponse>;
}
interface IDataSourceQueryServiceService_IGetById extends grpc.MethodDefinition<byhiras_datasource_datasource_pb.GetByIdRequest, byhiras_datasource_datasource_pb.DataSource> {
    path: "/byhiras.datasource.DataSourceQueryService/GetById";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<byhiras_datasource_datasource_pb.GetByIdRequest>;
    requestDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.GetByIdRequest>;
    responseSerialize: grpc.serialize<byhiras_datasource_datasource_pb.DataSource>;
    responseDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.DataSource>;
}
interface IDataSourceQueryServiceService_IListByIds extends grpc.MethodDefinition<byhiras_datasource_datasource_pb.ListByIdsRequest, byhiras_datasource_datasource_pb.ListResponse> {
    path: "/byhiras.datasource.DataSourceQueryService/ListByIds";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<byhiras_datasource_datasource_pb.ListByIdsRequest>;
    requestDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.ListByIdsRequest>;
    responseSerialize: grpc.serialize<byhiras_datasource_datasource_pb.ListResponse>;
    responseDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.ListResponse>;
}
interface IDataSourceQueryServiceService_IList extends grpc.MethodDefinition<byhiras_datasource_datasource_pb.ListRequest, byhiras_datasource_datasource_pb.ListResponse> {
    path: "/byhiras.datasource.DataSourceQueryService/List";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<byhiras_datasource_datasource_pb.ListRequest>;
    requestDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.ListRequest>;
    responseSerialize: grpc.serialize<byhiras_datasource_datasource_pb.ListResponse>;
    responseDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.ListResponse>;
}
interface IDataSourceQueryServiceService_IStreamPage extends grpc.MethodDefinition<byhiras_datasource_datasource_pb.PageRequest, byhiras_datasource_datasource_pb.StreamPageResponse> {
    path: "/byhiras.datasource.DataSourceQueryService/StreamPage";
    requestStream: false;
    responseStream: true;
    requestSerialize: grpc.serialize<byhiras_datasource_datasource_pb.PageRequest>;
    requestDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.PageRequest>;
    responseSerialize: grpc.serialize<byhiras_datasource_datasource_pb.StreamPageResponse>;
    responseDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.StreamPageResponse>;
}
interface IDataSourceQueryServiceService_IListContributors extends grpc.MethodDefinition<byhiras_datasource_datasource_pb.ListContributorsRequest, byhiras_datasource_datasource_pb.ListContributorsResponse> {
    path: "/byhiras.datasource.DataSourceQueryService/ListContributors";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<byhiras_datasource_datasource_pb.ListContributorsRequest>;
    requestDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.ListContributorsRequest>;
    responseSerialize: grpc.serialize<byhiras_datasource_datasource_pb.ListContributorsResponse>;
    responseDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.ListContributorsResponse>;
}

export const DataSourceQueryServiceService: IDataSourceQueryServiceService;

export interface IDataSourceQueryServiceServer extends grpc.UntypedServiceImplementation {
    getVersion: grpc.handleUnaryCall<byhiras_datasource_datasource_pb.GetVersionRequest, byhiras_datasource_datasource_pb.GetVersionResponse>;
    getById: grpc.handleUnaryCall<byhiras_datasource_datasource_pb.GetByIdRequest, byhiras_datasource_datasource_pb.DataSource>;
    listByIds: grpc.handleUnaryCall<byhiras_datasource_datasource_pb.ListByIdsRequest, byhiras_datasource_datasource_pb.ListResponse>;
    list: grpc.handleUnaryCall<byhiras_datasource_datasource_pb.ListRequest, byhiras_datasource_datasource_pb.ListResponse>;
    streamPage: grpc.handleServerStreamingCall<byhiras_datasource_datasource_pb.PageRequest, byhiras_datasource_datasource_pb.StreamPageResponse>;
    listContributors: grpc.handleUnaryCall<byhiras_datasource_datasource_pb.ListContributorsRequest, byhiras_datasource_datasource_pb.ListContributorsResponse>;
}

export interface IDataSourceQueryServiceClient {
    getVersion(request: byhiras_datasource_datasource_pb.GetVersionRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.GetVersionResponse) => void): grpc.ClientUnaryCall;
    getVersion(request: byhiras_datasource_datasource_pb.GetVersionRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.GetVersionResponse) => void): grpc.ClientUnaryCall;
    getVersion(request: byhiras_datasource_datasource_pb.GetVersionRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.GetVersionResponse) => void): grpc.ClientUnaryCall;
    getById(request: byhiras_datasource_datasource_pb.GetByIdRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.DataSource) => void): grpc.ClientUnaryCall;
    getById(request: byhiras_datasource_datasource_pb.GetByIdRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.DataSource) => void): grpc.ClientUnaryCall;
    getById(request: byhiras_datasource_datasource_pb.GetByIdRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.DataSource) => void): grpc.ClientUnaryCall;
    listByIds(request: byhiras_datasource_datasource_pb.ListByIdsRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    listByIds(request: byhiras_datasource_datasource_pb.ListByIdsRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    listByIds(request: byhiras_datasource_datasource_pb.ListByIdsRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    list(request: byhiras_datasource_datasource_pb.ListRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    list(request: byhiras_datasource_datasource_pb.ListRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    list(request: byhiras_datasource_datasource_pb.ListRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    streamPage(request: byhiras_datasource_datasource_pb.PageRequest, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<byhiras_datasource_datasource_pb.StreamPageResponse>;
    streamPage(request: byhiras_datasource_datasource_pb.PageRequest, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<byhiras_datasource_datasource_pb.StreamPageResponse>;
    listContributors(request: byhiras_datasource_datasource_pb.ListContributorsRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListContributorsResponse) => void): grpc.ClientUnaryCall;
    listContributors(request: byhiras_datasource_datasource_pb.ListContributorsRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListContributorsResponse) => void): grpc.ClientUnaryCall;
    listContributors(request: byhiras_datasource_datasource_pb.ListContributorsRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListContributorsResponse) => void): grpc.ClientUnaryCall;
}

export class DataSourceQueryServiceClient extends grpc.Client implements IDataSourceQueryServiceClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public getVersion(request: byhiras_datasource_datasource_pb.GetVersionRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.GetVersionResponse) => void): grpc.ClientUnaryCall;
    public getVersion(request: byhiras_datasource_datasource_pb.GetVersionRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.GetVersionResponse) => void): grpc.ClientUnaryCall;
    public getVersion(request: byhiras_datasource_datasource_pb.GetVersionRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.GetVersionResponse) => void): grpc.ClientUnaryCall;
    public getById(request: byhiras_datasource_datasource_pb.GetByIdRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.DataSource) => void): grpc.ClientUnaryCall;
    public getById(request: byhiras_datasource_datasource_pb.GetByIdRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.DataSource) => void): grpc.ClientUnaryCall;
    public getById(request: byhiras_datasource_datasource_pb.GetByIdRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.DataSource) => void): grpc.ClientUnaryCall;
    public listByIds(request: byhiras_datasource_datasource_pb.ListByIdsRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    public listByIds(request: byhiras_datasource_datasource_pb.ListByIdsRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    public listByIds(request: byhiras_datasource_datasource_pb.ListByIdsRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    public list(request: byhiras_datasource_datasource_pb.ListRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    public list(request: byhiras_datasource_datasource_pb.ListRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    public list(request: byhiras_datasource_datasource_pb.ListRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListResponse) => void): grpc.ClientUnaryCall;
    public streamPage(request: byhiras_datasource_datasource_pb.PageRequest, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<byhiras_datasource_datasource_pb.StreamPageResponse>;
    public streamPage(request: byhiras_datasource_datasource_pb.PageRequest, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<byhiras_datasource_datasource_pb.StreamPageResponse>;
    public listContributors(request: byhiras_datasource_datasource_pb.ListContributorsRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListContributorsResponse) => void): grpc.ClientUnaryCall;
    public listContributors(request: byhiras_datasource_datasource_pb.ListContributorsRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListContributorsResponse) => void): grpc.ClientUnaryCall;
    public listContributors(request: byhiras_datasource_datasource_pb.ListContributorsRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.ListContributorsResponse) => void): grpc.ClientUnaryCall;
}

interface IDataSourceCommandServiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    create: IDataSourceCommandServiceService_ICreate;
    update: IDataSourceCommandServiceService_IUpdate;
    generateSshKeys: IDataSourceCommandServiceService_IGenerateSshKeys;
}

interface IDataSourceCommandServiceService_ICreate extends grpc.MethodDefinition<byhiras_delta_json_patch_pb.JsonPatchRequest, byhiras_datasource_datasource_pb.CreateResponse> {
    path: "/byhiras.datasource.DataSourceCommandService/Create";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<byhiras_delta_json_patch_pb.JsonPatchRequest>;
    requestDeserialize: grpc.deserialize<byhiras_delta_json_patch_pb.JsonPatchRequest>;
    responseSerialize: grpc.serialize<byhiras_datasource_datasource_pb.CreateResponse>;
    responseDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.CreateResponse>;
}
interface IDataSourceCommandServiceService_IUpdate extends grpc.MethodDefinition<byhiras_delta_json_patch_pb.JsonPatchRequest, byhiras_datasource_datasource_pb.UpdateResponse> {
    path: "/byhiras.datasource.DataSourceCommandService/Update";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<byhiras_delta_json_patch_pb.JsonPatchRequest>;
    requestDeserialize: grpc.deserialize<byhiras_delta_json_patch_pb.JsonPatchRequest>;
    responseSerialize: grpc.serialize<byhiras_datasource_datasource_pb.UpdateResponse>;
    responseDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.UpdateResponse>;
}
interface IDataSourceCommandServiceService_IGenerateSshKeys extends grpc.MethodDefinition<byhiras_delta_json_patch_pb.JsonPatchRequest, byhiras_datasource_datasource_pb.UpdateResponse> {
    path: "/byhiras.datasource.DataSourceCommandService/GenerateSshKeys";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<byhiras_delta_json_patch_pb.JsonPatchRequest>;
    requestDeserialize: grpc.deserialize<byhiras_delta_json_patch_pb.JsonPatchRequest>;
    responseSerialize: grpc.serialize<byhiras_datasource_datasource_pb.UpdateResponse>;
    responseDeserialize: grpc.deserialize<byhiras_datasource_datasource_pb.UpdateResponse>;
}

export const DataSourceCommandServiceService: IDataSourceCommandServiceService;

export interface IDataSourceCommandServiceServer extends grpc.UntypedServiceImplementation {
    create: grpc.handleUnaryCall<byhiras_delta_json_patch_pb.JsonPatchRequest, byhiras_datasource_datasource_pb.CreateResponse>;
    update: grpc.handleUnaryCall<byhiras_delta_json_patch_pb.JsonPatchRequest, byhiras_datasource_datasource_pb.UpdateResponse>;
    generateSshKeys: grpc.handleUnaryCall<byhiras_delta_json_patch_pb.JsonPatchRequest, byhiras_datasource_datasource_pb.UpdateResponse>;
}

export interface IDataSourceCommandServiceClient {
    create(request: byhiras_delta_json_patch_pb.JsonPatchRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.CreateResponse) => void): grpc.ClientUnaryCall;
    create(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.CreateResponse) => void): grpc.ClientUnaryCall;
    create(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.CreateResponse) => void): grpc.ClientUnaryCall;
    update(request: byhiras_delta_json_patch_pb.JsonPatchRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    update(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    update(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    generateSshKeys(request: byhiras_delta_json_patch_pb.JsonPatchRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    generateSshKeys(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    generateSshKeys(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
}

export class DataSourceCommandServiceClient extends grpc.Client implements IDataSourceCommandServiceClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public create(request: byhiras_delta_json_patch_pb.JsonPatchRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.CreateResponse) => void): grpc.ClientUnaryCall;
    public create(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.CreateResponse) => void): grpc.ClientUnaryCall;
    public create(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.CreateResponse) => void): grpc.ClientUnaryCall;
    public update(request: byhiras_delta_json_patch_pb.JsonPatchRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    public update(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    public update(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    public generateSshKeys(request: byhiras_delta_json_patch_pb.JsonPatchRequest, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    public generateSshKeys(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
    public generateSshKeys(request: byhiras_delta_json_patch_pb.JsonPatchRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: byhiras_datasource_datasource_pb.UpdateResponse) => void): grpc.ClientUnaryCall;
}
