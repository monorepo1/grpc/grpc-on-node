// source: byhiras/datasource/service.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = (function() {
  if (this) { return this; }
  if (typeof window !== 'undefined') { return window; }
  if (typeof global !== 'undefined') { return global; }
  if (typeof self !== 'undefined') { return self; }
  return Function('return this')();
}.call(null));

var byhiras_datasource_datasource_pb = require('../../byhiras/datasource/datasource_pb.js');
goog.object.extend(proto, byhiras_datasource_datasource_pb);
var byhiras_delta_json_patch_pb = require('../../byhiras/delta/json_patch_pb.js');
goog.object.extend(proto, byhiras_delta_json_patch_pb);
