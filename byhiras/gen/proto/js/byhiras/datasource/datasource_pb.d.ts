// package: byhiras.datasource
// file: byhiras/datasource/datasource.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";
import * as byhiras_identity_identity_pb from "../../byhiras/identity/identity_pb";

export class DataSource extends jspb.Message { 
    getDescription(): string;
    setDescription(value: string): DataSource;

    hasIdentity(): boolean;
    clearIdentity(): void;
    getIdentity(): byhiras_identity_identity_pb.Identity | undefined;
    setIdentity(value?: byhiras_identity_identity_pb.Identity): DataSource;

    hasContributor(): boolean;
    clearContributor(): void;
    getContributor(): byhiras_identity_identity_pb.Identity | undefined;
    setContributor(value?: byhiras_identity_identity_pb.Identity): DataSource;

    hasDeleted(): boolean;
    clearDeleted(): void;
    getDeleted(): boolean | undefined;
    setDeleted(value: boolean): DataSource;

    hasLastUpdateUser(): boolean;
    clearLastUpdateUser(): void;
    getLastUpdateUser(): string | undefined;
    setLastUpdateUser(value: string): DataSource;

    hasLastUpdateTime(): boolean;
    clearLastUpdateTime(): void;
    getLastUpdateTime(): number | undefined;
    setLastUpdateTime(value: number): DataSource;

    hasSftpConfig(): boolean;
    clearSftpConfig(): void;
    getSftpConfig(): SftpConfig | undefined;
    setSftpConfig(value?: SftpConfig): DataSource;

    hasHttpsConfig(): boolean;
    clearHttpsConfig(): void;
    getHttpsConfig(): HttpsConfig | undefined;
    setHttpsConfig(value?: HttpsConfig): DataSource;

    hasImapConfig(): boolean;
    clearImapConfig(): void;
    getImapConfig(): ImapConfig | undefined;
    setImapConfig(value?: ImapConfig): DataSource;

    hasHdfsConfig(): boolean;
    clearHdfsConfig(): void;
    getHdfsConfig(): HdfsConfig | undefined;
    setHdfsConfig(value?: HdfsConfig): DataSource;

    hasStatus(): boolean;
    clearStatus(): void;
    getStatus(): DataSourceStatus | undefined;
    setStatus(value: DataSourceStatus): DataSource;

    hasErrorMessage(): boolean;
    clearErrorMessage(): void;
    getErrorMessage(): string | undefined;
    setErrorMessage(value: string): DataSource;

    hasVersionhash(): boolean;
    clearVersionhash(): void;
    getVersionhash(): string | undefined;
    setVersionhash(value: string): DataSource;

    getConfigCase(): DataSource.ConfigCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DataSource.AsObject;
    static toObject(includeInstance: boolean, msg: DataSource): DataSource.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DataSource, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DataSource;
    static deserializeBinaryFromReader(message: DataSource, reader: jspb.BinaryReader): DataSource;
}

export namespace DataSource {
    export type AsObject = {
        description: string,
        identity?: byhiras_identity_identity_pb.Identity.AsObject,
        contributor?: byhiras_identity_identity_pb.Identity.AsObject,
        deleted?: boolean,
        lastUpdateUser?: string,
        lastUpdateTime?: number,
        sftpConfig?: SftpConfig.AsObject,
        httpsConfig?: HttpsConfig.AsObject,
        imapConfig?: ImapConfig.AsObject,
        hdfsConfig?: HdfsConfig.AsObject,
        status?: DataSourceStatus,
        errorMessage?: string,
        versionhash?: string,
    }

    export enum ConfigCase {
        CONFIG_NOT_SET = 0,
        SFTP_CONFIG = 7,
        HTTPS_CONFIG = 8,
        IMAP_CONFIG = 9,
        HDFS_CONFIG = 13,
    }

}

export class SftpConfig extends jspb.Message { 
    getUser(): string;
    setUser(value: string): SftpConfig;

    hasPassword(): boolean;
    clearPassword(): void;
    getPassword(): string | undefined;
    setPassword(value: string): SftpConfig;
    getHost(): string;
    setHost(value: string): SftpConfig;
    getPort(): number;
    setPort(value: number): SftpConfig;
    clearExcludedPathsList(): void;
    getExcludedPathsList(): Array<string>;
    setExcludedPathsList(value: Array<string>): SftpConfig;
    addExcludedPaths(value: string, index?: number): string;

    hasDeleteRemote(): boolean;
    clearDeleteRemote(): void;
    getDeleteRemote(): boolean | undefined;
    setDeleteRemote(value: boolean): SftpConfig;

    hasPublicKey(): boolean;
    clearPublicKey(): void;
    getPublicKey(): string | undefined;
    setPublicKey(value: string): SftpConfig;

    hasPrivateKey(): boolean;
    clearPrivateKey(): void;
    getPrivateKey(): string | undefined;
    setPrivateKey(value: string): SftpConfig;

    hasSalt(): boolean;
    clearSalt(): void;
    getSalt(): string | undefined;
    setSalt(value: string): SftpConfig;
    getAuthenticationMode(): SftpAuthenticationMode;
    setAuthenticationMode(value: SftpAuthenticationMode): SftpConfig;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SftpConfig.AsObject;
    static toObject(includeInstance: boolean, msg: SftpConfig): SftpConfig.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SftpConfig, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SftpConfig;
    static deserializeBinaryFromReader(message: SftpConfig, reader: jspb.BinaryReader): SftpConfig;
}

export namespace SftpConfig {
    export type AsObject = {
        user: string,
        password?: string,
        host: string,
        port: number,
        excludedPathsList: Array<string>,
        deleteRemote?: boolean,
        publicKey?: string,
        privateKey?: string,
        salt?: string,
        authenticationMode: SftpAuthenticationMode,
    }
}

export class HttpsConfig extends jspb.Message { 

    hasUser(): boolean;
    clearUser(): void;
    getUser(): string | undefined;
    setUser(value: string): HttpsConfig;

    hasPassword(): boolean;
    clearPassword(): void;
    getPassword(): string | undefined;
    setPassword(value: string): HttpsConfig;
    getHost(): string;
    setHost(value: string): HttpsConfig;
    getPort(): number;
    setPort(value: number): HttpsConfig;
    clearIncludedPathsList(): void;
    getIncludedPathsList(): Array<string>;
    setIncludedPathsList(value: Array<string>): HttpsConfig;
    addIncludedPaths(value: string, index?: number): string;

    hasSalt(): boolean;
    clearSalt(): void;
    getSalt(): string | undefined;
    setSalt(value: string): HttpsConfig;

    hasPublicKey(): boolean;
    clearPublicKey(): void;
    getPublicKey(): string | undefined;
    setPublicKey(value: string): HttpsConfig;

    hasPrivateKey(): boolean;
    clearPrivateKey(): void;
    getPrivateKey(): string | undefined;
    setPrivateKey(value: string): HttpsConfig;
    getAuthenticationMode(): HttpsAuthenticationMode;
    setAuthenticationMode(value: HttpsAuthenticationMode): HttpsConfig;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): HttpsConfig.AsObject;
    static toObject(includeInstance: boolean, msg: HttpsConfig): HttpsConfig.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: HttpsConfig, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): HttpsConfig;
    static deserializeBinaryFromReader(message: HttpsConfig, reader: jspb.BinaryReader): HttpsConfig;
}

export namespace HttpsConfig {
    export type AsObject = {
        user?: string,
        password?: string,
        host: string,
        port: number,
        includedPathsList: Array<string>,
        salt?: string,
        publicKey?: string,
        privateKey?: string,
        authenticationMode: HttpsAuthenticationMode,
    }
}

export class ImapConfig extends jspb.Message { 
    getUser(): string;
    setUser(value: string): ImapConfig;
    getPassword(): string;
    setPassword(value: string): ImapConfig;

    hasHost(): boolean;
    clearHost(): void;
    getHost(): string | undefined;
    setHost(value: string): ImapConfig;

    hasPort(): boolean;
    clearPort(): void;
    getPort(): number | undefined;
    setPort(value: number): ImapConfig;
    clearFoldersList(): void;
    getFoldersList(): Array<string>;
    setFoldersList(value: Array<string>): ImapConfig;
    addFolders(value: string, index?: number): string;

    hasDeleteRemote(): boolean;
    clearDeleteRemote(): void;
    getDeleteRemote(): boolean | undefined;
    setDeleteRemote(value: boolean): ImapConfig;
    clearExcludedFilePatternsList(): void;
    getExcludedFilePatternsList(): Array<string>;
    setExcludedFilePatternsList(value: Array<string>): ImapConfig;
    addExcludedFilePatterns(value: string, index?: number): string;

    hasSalt(): boolean;
    clearSalt(): void;
    getSalt(): string | undefined;
    setSalt(value: string): ImapConfig;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ImapConfig.AsObject;
    static toObject(includeInstance: boolean, msg: ImapConfig): ImapConfig.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ImapConfig, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ImapConfig;
    static deserializeBinaryFromReader(message: ImapConfig, reader: jspb.BinaryReader): ImapConfig;
}

export namespace ImapConfig {
    export type AsObject = {
        user: string,
        password: string,
        host?: string,
        port?: number,
        foldersList: Array<string>,
        deleteRemote?: boolean,
        excludedFilePatternsList: Array<string>,
        salt?: string,
    }
}

export class HdfsConfig extends jspb.Message { 
    getHost(): string;
    setHost(value: string): HdfsConfig;
    getPort(): number;
    setPort(value: number): HdfsConfig;
    getPath(): string;
    setPath(value: string): HdfsConfig;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): HdfsConfig.AsObject;
    static toObject(includeInstance: boolean, msg: HdfsConfig): HdfsConfig.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: HdfsConfig, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): HdfsConfig;
    static deserializeBinaryFromReader(message: HdfsConfig, reader: jspb.BinaryReader): HdfsConfig;
}

export namespace HdfsConfig {
    export type AsObject = {
        host: string,
        port: number,
        path: string,
    }
}

export class CreateResponse extends jspb.Message { 

    hasCreatedIdentity(): boolean;
    clearCreatedIdentity(): void;
    getCreatedIdentity(): byhiras_identity_identity_pb.Identity | undefined;
    setCreatedIdentity(value?: byhiras_identity_identity_pb.Identity): CreateResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): CreateResponse.AsObject;
    static toObject(includeInstance: boolean, msg: CreateResponse): CreateResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: CreateResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): CreateResponse;
    static deserializeBinaryFromReader(message: CreateResponse, reader: jspb.BinaryReader): CreateResponse;
}

export namespace CreateResponse {
    export type AsObject = {
        createdIdentity?: byhiras_identity_identity_pb.Identity.AsObject,
    }
}

export class UpdateResponse extends jspb.Message { 

    hasUpdatedIdentity(): boolean;
    clearUpdatedIdentity(): void;
    getUpdatedIdentity(): byhiras_identity_identity_pb.Identity | undefined;
    setUpdatedIdentity(value?: byhiras_identity_identity_pb.Identity): UpdateResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UpdateResponse.AsObject;
    static toObject(includeInstance: boolean, msg: UpdateResponse): UpdateResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UpdateResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UpdateResponse;
    static deserializeBinaryFromReader(message: UpdateResponse, reader: jspb.BinaryReader): UpdateResponse;
}

export namespace UpdateResponse {
    export type AsObject = {
        updatedIdentity?: byhiras_identity_identity_pb.Identity.AsObject,
    }
}

export class GetByIdRequest extends jspb.Message { 

    hasId(): boolean;
    clearId(): void;
    getId(): byhiras_identity_identity_pb.Identity | undefined;
    setId(value?: byhiras_identity_identity_pb.Identity): GetByIdRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetByIdRequest.AsObject;
    static toObject(includeInstance: boolean, msg: GetByIdRequest): GetByIdRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetByIdRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetByIdRequest;
    static deserializeBinaryFromReader(message: GetByIdRequest, reader: jspb.BinaryReader): GetByIdRequest;
}

export namespace GetByIdRequest {
    export type AsObject = {
        id?: byhiras_identity_identity_pb.Identity.AsObject,
    }
}

export class GetVersionRequest extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetVersionRequest.AsObject;
    static toObject(includeInstance: boolean, msg: GetVersionRequest): GetVersionRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetVersionRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetVersionRequest;
    static deserializeBinaryFromReader(message: GetVersionRequest, reader: jspb.BinaryReader): GetVersionRequest;
}

export namespace GetVersionRequest {
    export type AsObject = {
    }
}

export class GetVersionResponse extends jspb.Message { 
    getVersion(): string;
    setVersion(value: string): GetVersionResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetVersionResponse.AsObject;
    static toObject(includeInstance: boolean, msg: GetVersionResponse): GetVersionResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetVersionResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetVersionResponse;
    static deserializeBinaryFromReader(message: GetVersionResponse, reader: jspb.BinaryReader): GetVersionResponse;
}

export namespace GetVersionResponse {
    export type AsObject = {
        version: string,
    }
}

export class PageRequest extends jspb.Message { 

    hasContributor(): boolean;
    clearContributor(): void;
    getContributor(): byhiras_identity_identity_pb.Identity | undefined;
    setContributor(value?: byhiras_identity_identity_pb.Identity): PageRequest;
    getPageSize(): number;
    setPageSize(value: number): PageRequest;
    getPageOffset(): number;
    setPageOffset(value: number): PageRequest;

    hasFilterByDeleted(): boolean;
    clearFilterByDeleted(): void;
    getFilterByDeleted(): ListRequestDeletedFilter | undefined;
    setFilterByDeleted(value: ListRequestDeletedFilter): PageRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PageRequest.AsObject;
    static toObject(includeInstance: boolean, msg: PageRequest): PageRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PageRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PageRequest;
    static deserializeBinaryFromReader(message: PageRequest, reader: jspb.BinaryReader): PageRequest;
}

export namespace PageRequest {
    export type AsObject = {
        contributor?: byhiras_identity_identity_pb.Identity.AsObject,
        pageSize: number,
        pageOffset: number,
        filterByDeleted?: ListRequestDeletedFilter,
    }
}

export class StreamPageResponse extends jspb.Message { 

    hasDataSource(): boolean;
    clearDataSource(): void;
    getDataSource(): DataSource | undefined;
    setDataSource(value?: DataSource): StreamPageResponse;
    getTotalElements(): number;
    setTotalElements(value: number): StreamPageResponse;
    getIncludedInFilter(): boolean;
    setIncludedInFilter(value: boolean): StreamPageResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): StreamPageResponse.AsObject;
    static toObject(includeInstance: boolean, msg: StreamPageResponse): StreamPageResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: StreamPageResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): StreamPageResponse;
    static deserializeBinaryFromReader(message: StreamPageResponse, reader: jspb.BinaryReader): StreamPageResponse;
}

export namespace StreamPageResponse {
    export type AsObject = {
        dataSource?: DataSource.AsObject,
        totalElements: number,
        includedInFilter: boolean,
    }
}

export class ListResponse extends jspb.Message { 
    clearDataSourcesList(): void;
    getDataSourcesList(): Array<DataSource>;
    setDataSourcesList(value: Array<DataSource>): ListResponse;
    addDataSources(value?: DataSource, index?: number): DataSource;
    getTotalElements(): number;
    setTotalElements(value: number): ListResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ListResponse.AsObject;
    static toObject(includeInstance: boolean, msg: ListResponse): ListResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ListResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ListResponse;
    static deserializeBinaryFromReader(message: ListResponse, reader: jspb.BinaryReader): ListResponse;
}

export namespace ListResponse {
    export type AsObject = {
        dataSourcesList: Array<DataSource.AsObject>,
        totalElements: number,
    }
}

export class ListByIdsRequest extends jspb.Message { 
    clearIdentitiesList(): void;
    getIdentitiesList(): Array<number>;
    setIdentitiesList(value: Array<number>): ListByIdsRequest;
    addIdentities(value: number, index?: number): number;
    getPageSize(): number;
    setPageSize(value: number): ListByIdsRequest;
    getPageOffset(): number;
    setPageOffset(value: number): ListByIdsRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ListByIdsRequest.AsObject;
    static toObject(includeInstance: boolean, msg: ListByIdsRequest): ListByIdsRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ListByIdsRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ListByIdsRequest;
    static deserializeBinaryFromReader(message: ListByIdsRequest, reader: jspb.BinaryReader): ListByIdsRequest;
}

export namespace ListByIdsRequest {
    export type AsObject = {
        identitiesList: Array<number>,
        pageSize: number,
        pageOffset: number,
    }
}

export class ListRequest extends jspb.Message { 

    hasContributor(): boolean;
    clearContributor(): void;
    getContributor(): byhiras_identity_identity_pb.Identity | undefined;
    setContributor(value?: byhiras_identity_identity_pb.Identity): ListRequest;
    getPageSize(): number;
    setPageSize(value: number): ListRequest;
    getPageOffset(): number;
    setPageOffset(value: number): ListRequest;

    hasIncludeDeleted(): boolean;
    clearIncludeDeleted(): void;
    getIncludeDeleted(): boolean | undefined;
    setIncludeDeleted(value: boolean): ListRequest;

    hasFilterByDeleted(): boolean;
    clearFilterByDeleted(): void;
    getFilterByDeleted(): ListRequestDeletedFilter | undefined;
    setFilterByDeleted(value: ListRequestDeletedFilter): ListRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ListRequest.AsObject;
    static toObject(includeInstance: boolean, msg: ListRequest): ListRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ListRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ListRequest;
    static deserializeBinaryFromReader(message: ListRequest, reader: jspb.BinaryReader): ListRequest;
}

export namespace ListRequest {
    export type AsObject = {
        contributor?: byhiras_identity_identity_pb.Identity.AsObject,
        pageSize: number,
        pageOffset: number,
        includeDeleted?: boolean,
        filterByDeleted?: ListRequestDeletedFilter,
    }
}

export class ListContributorsRequest extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ListContributorsRequest.AsObject;
    static toObject(includeInstance: boolean, msg: ListContributorsRequest): ListContributorsRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ListContributorsRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ListContributorsRequest;
    static deserializeBinaryFromReader(message: ListContributorsRequest, reader: jspb.BinaryReader): ListContributorsRequest;
}

export namespace ListContributorsRequest {
    export type AsObject = {
    }
}

export class ListContributorsResponse extends jspb.Message { 
    clearContributorsList(): void;
    getContributorsList(): Array<byhiras_identity_identity_pb.Identity>;
    setContributorsList(value: Array<byhiras_identity_identity_pb.Identity>): ListContributorsResponse;
    addContributors(value?: byhiras_identity_identity_pb.Identity, index?: number): byhiras_identity_identity_pb.Identity;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ListContributorsResponse.AsObject;
    static toObject(includeInstance: boolean, msg: ListContributorsResponse): ListContributorsResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ListContributorsResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ListContributorsResponse;
    static deserializeBinaryFromReader(message: ListContributorsResponse, reader: jspb.BinaryReader): ListContributorsResponse;
}

export namespace ListContributorsResponse {
    export type AsObject = {
        contributorsList: Array<byhiras_identity_identity_pb.Identity.AsObject>,
    }
}

export enum DataSourceStatus {
    DATA_SOURCE_STATUS_UNSPECIFIED = 0,
    DATA_SOURCE_STATUS_UNVERIFIED = 1,
    DATA_SOURCE_STATUS_OK = 2,
    DATA_SOURCE_STATUS_FAILED = 3,
}

export enum SftpAuthenticationMode {
    SFTP_AUTHENTICATION_MODE_UNSPECIFIED = 0,
    SFTP_AUTHENTICATION_MODE_PASSWORD = 1,
    SFTP_AUTHENTICATION_MODE_SSH_KEY = 2,
}

export enum HttpsAuthenticationMode {
    HTTPS_AUTHENTICATION_MODE_UNSPECIFIED = 0,
    HTTPS_AUTHENTICATION_MODE_PASSWORD = 1,
    HTTPS_AUTHENTICATION_MODE_SSH_KEY = 2,
    HTTPS_AUTHENTICATION_MODE_NONE = 3,
}

export enum ListRequestDeletedFilter {
    LIST_REQUEST_DELETED_FILTER_UNSPECIFIED = 0,
    LIST_REQUEST_DELETED_FILTER_ALL = 1,
    LIST_REQUEST_DELETED_FILTER_DELETED = 2,
    LIST_REQUEST_DELETED_FILTER_NOT_DELETED = 3,
}
