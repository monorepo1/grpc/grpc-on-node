// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var byhiras_datasource_datasource_pb = require('../../byhiras/datasource/datasource_pb.js');
var byhiras_delta_json_patch_pb = require('../../byhiras/delta/json_patch_pb.js');

function serialize_byhiras_datasource_CreateResponse(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.CreateResponse)) {
    throw new Error('Expected argument of type byhiras.datasource.CreateResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_CreateResponse(buffer_arg) {
  return byhiras_datasource_datasource_pb.CreateResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_DataSource(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.DataSource)) {
    throw new Error('Expected argument of type byhiras.datasource.DataSource');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_DataSource(buffer_arg) {
  return byhiras_datasource_datasource_pb.DataSource.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_GetByIdRequest(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.GetByIdRequest)) {
    throw new Error('Expected argument of type byhiras.datasource.GetByIdRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_GetByIdRequest(buffer_arg) {
  return byhiras_datasource_datasource_pb.GetByIdRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_GetVersionRequest(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.GetVersionRequest)) {
    throw new Error('Expected argument of type byhiras.datasource.GetVersionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_GetVersionRequest(buffer_arg) {
  return byhiras_datasource_datasource_pb.GetVersionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_GetVersionResponse(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.GetVersionResponse)) {
    throw new Error('Expected argument of type byhiras.datasource.GetVersionResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_GetVersionResponse(buffer_arg) {
  return byhiras_datasource_datasource_pb.GetVersionResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_ListByIdsRequest(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.ListByIdsRequest)) {
    throw new Error('Expected argument of type byhiras.datasource.ListByIdsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_ListByIdsRequest(buffer_arg) {
  return byhiras_datasource_datasource_pb.ListByIdsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_ListContributorsRequest(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.ListContributorsRequest)) {
    throw new Error('Expected argument of type byhiras.datasource.ListContributorsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_ListContributorsRequest(buffer_arg) {
  return byhiras_datasource_datasource_pb.ListContributorsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_ListContributorsResponse(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.ListContributorsResponse)) {
    throw new Error('Expected argument of type byhiras.datasource.ListContributorsResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_ListContributorsResponse(buffer_arg) {
  return byhiras_datasource_datasource_pb.ListContributorsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_ListRequest(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.ListRequest)) {
    throw new Error('Expected argument of type byhiras.datasource.ListRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_ListRequest(buffer_arg) {
  return byhiras_datasource_datasource_pb.ListRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_ListResponse(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.ListResponse)) {
    throw new Error('Expected argument of type byhiras.datasource.ListResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_ListResponse(buffer_arg) {
  return byhiras_datasource_datasource_pb.ListResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_PageRequest(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.PageRequest)) {
    throw new Error('Expected argument of type byhiras.datasource.PageRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_PageRequest(buffer_arg) {
  return byhiras_datasource_datasource_pb.PageRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_StreamPageResponse(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.StreamPageResponse)) {
    throw new Error('Expected argument of type byhiras.datasource.StreamPageResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_StreamPageResponse(buffer_arg) {
  return byhiras_datasource_datasource_pb.StreamPageResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_datasource_UpdateResponse(arg) {
  if (!(arg instanceof byhiras_datasource_datasource_pb.UpdateResponse)) {
    throw new Error('Expected argument of type byhiras.datasource.UpdateResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_datasource_UpdateResponse(buffer_arg) {
  return byhiras_datasource_datasource_pb.UpdateResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_byhiras_delta_JsonPatchRequest(arg) {
  if (!(arg instanceof byhiras_delta_json_patch_pb.JsonPatchRequest)) {
    throw new Error('Expected argument of type byhiras.delta.JsonPatchRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_byhiras_delta_JsonPatchRequest(buffer_arg) {
  return byhiras_delta_json_patch_pb.JsonPatchRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


var DataSourceQueryServiceService = exports.DataSourceQueryServiceService = {
  getVersion: {
    path: '/byhiras.datasource.DataSourceQueryService/GetVersion',
    requestStream: false,
    responseStream: false,
    requestType: byhiras_datasource_datasource_pb.GetVersionRequest,
    responseType: byhiras_datasource_datasource_pb.GetVersionResponse,
    requestSerialize: serialize_byhiras_datasource_GetVersionRequest,
    requestDeserialize: deserialize_byhiras_datasource_GetVersionRequest,
    responseSerialize: serialize_byhiras_datasource_GetVersionResponse,
    responseDeserialize: deserialize_byhiras_datasource_GetVersionResponse,
  },
  getById: {
    path: '/byhiras.datasource.DataSourceQueryService/GetById',
    requestStream: false,
    responseStream: false,
    requestType: byhiras_datasource_datasource_pb.GetByIdRequest,
    responseType: byhiras_datasource_datasource_pb.DataSource,
    requestSerialize: serialize_byhiras_datasource_GetByIdRequest,
    requestDeserialize: deserialize_byhiras_datasource_GetByIdRequest,
    responseSerialize: serialize_byhiras_datasource_DataSource,
    responseDeserialize: deserialize_byhiras_datasource_DataSource,
  },
  listByIds: {
    path: '/byhiras.datasource.DataSourceQueryService/ListByIds',
    requestStream: false,
    responseStream: false,
    requestType: byhiras_datasource_datasource_pb.ListByIdsRequest,
    responseType: byhiras_datasource_datasource_pb.ListResponse,
    requestSerialize: serialize_byhiras_datasource_ListByIdsRequest,
    requestDeserialize: deserialize_byhiras_datasource_ListByIdsRequest,
    responseSerialize: serialize_byhiras_datasource_ListResponse,
    responseDeserialize: deserialize_byhiras_datasource_ListResponse,
  },
  list: {
    path: '/byhiras.datasource.DataSourceQueryService/List',
    requestStream: false,
    responseStream: false,
    requestType: byhiras_datasource_datasource_pb.ListRequest,
    responseType: byhiras_datasource_datasource_pb.ListResponse,
    requestSerialize: serialize_byhiras_datasource_ListRequest,
    requestDeserialize: deserialize_byhiras_datasource_ListRequest,
    responseSerialize: serialize_byhiras_datasource_ListResponse,
    responseDeserialize: deserialize_byhiras_datasource_ListResponse,
  },
  //
// StreamPage is similar to List, except the response is a series individual datasource update
// i.e. if you asked for 10 in a page then you would get 10 notifications (assuming there are 10 available.
// The response stream stays open and will receive a new response for each datasource that has been updated that was
// on the original page response
streamPage: {
    path: '/byhiras.datasource.DataSourceQueryService/StreamPage',
    requestStream: false,
    responseStream: true,
    requestType: byhiras_datasource_datasource_pb.PageRequest,
    responseType: byhiras_datasource_datasource_pb.StreamPageResponse,
    requestSerialize: serialize_byhiras_datasource_PageRequest,
    requestDeserialize: deserialize_byhiras_datasource_PageRequest,
    responseSerialize: serialize_byhiras_datasource_StreamPageResponse,
    responseDeserialize: deserialize_byhiras_datasource_StreamPageResponse,
  },
  listContributors: {
    path: '/byhiras.datasource.DataSourceQueryService/ListContributors',
    requestStream: false,
    responseStream: false,
    requestType: byhiras_datasource_datasource_pb.ListContributorsRequest,
    responseType: byhiras_datasource_datasource_pb.ListContributorsResponse,
    requestSerialize: serialize_byhiras_datasource_ListContributorsRequest,
    requestDeserialize: deserialize_byhiras_datasource_ListContributorsRequest,
    responseSerialize: serialize_byhiras_datasource_ListContributorsResponse,
    responseDeserialize: deserialize_byhiras_datasource_ListContributorsResponse,
  },
};

exports.DataSourceQueryServiceClient = grpc.makeGenericClientConstructor(DataSourceQueryServiceService);
var DataSourceCommandServiceService = exports.DataSourceCommandServiceService = {
  create: {
    path: '/byhiras.datasource.DataSourceCommandService/Create',
    requestStream: false,
    responseStream: false,
    requestType: byhiras_delta_json_patch_pb.JsonPatchRequest,
    responseType: byhiras_datasource_datasource_pb.CreateResponse,
    requestSerialize: serialize_byhiras_delta_JsonPatchRequest,
    requestDeserialize: deserialize_byhiras_delta_JsonPatchRequest,
    responseSerialize: serialize_byhiras_datasource_CreateResponse,
    responseDeserialize: deserialize_byhiras_datasource_CreateResponse,
  },
  update: {
    path: '/byhiras.datasource.DataSourceCommandService/Update',
    requestStream: false,
    responseStream: false,
    requestType: byhiras_delta_json_patch_pb.JsonPatchRequest,
    responseType: byhiras_datasource_datasource_pb.UpdateResponse,
    requestSerialize: serialize_byhiras_delta_JsonPatchRequest,
    requestDeserialize: deserialize_byhiras_delta_JsonPatchRequest,
    responseSerialize: serialize_byhiras_datasource_UpdateResponse,
    responseDeserialize: deserialize_byhiras_datasource_UpdateResponse,
  },
  generateSshKeys: {
    path: '/byhiras.datasource.DataSourceCommandService/GenerateSshKeys',
    requestStream: false,
    responseStream: false,
    requestType: byhiras_delta_json_patch_pb.JsonPatchRequest,
    responseType: byhiras_datasource_datasource_pb.UpdateResponse,
    requestSerialize: serialize_byhiras_delta_JsonPatchRequest,
    requestDeserialize: deserialize_byhiras_delta_JsonPatchRequest,
    responseSerialize: serialize_byhiras_datasource_UpdateResponse,
    responseDeserialize: deserialize_byhiras_datasource_UpdateResponse,
  },
};

exports.DataSourceCommandServiceClient = grpc.makeGenericClientConstructor(DataSourceCommandServiceService);
