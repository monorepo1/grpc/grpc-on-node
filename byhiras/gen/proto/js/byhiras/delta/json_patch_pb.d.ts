// package: byhiras.delta
// file: byhiras/delta/json_patch.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";
import * as byhiras_identity_identity_pb from "../../byhiras/identity/identity_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";

export class Value extends jspb.Message { 

    hasString(): boolean;
    clearString(): void;
    getString(): string;
    setString(value: string): Value;

    hasBool(): boolean;
    clearBool(): void;
    getBool(): boolean;
    setBool(value: boolean): Value;

    hasInt(): boolean;
    clearInt(): void;
    getInt(): number;
    setInt(value: number): Value;

    hasLong(): boolean;
    clearLong(): void;
    getLong(): number;
    setLong(value: number): Value;

    hasFloat(): boolean;
    clearFloat(): void;
    getFloat(): number;
    setFloat(value: number): Value;

    hasDouble(): boolean;
    clearDouble(): void;
    getDouble(): number;
    setDouble(value: number): Value;

    hasBytes(): boolean;
    clearBytes(): void;
    getBytes(): Uint8Array | string;
    getBytes_asU8(): Uint8Array;
    getBytes_asB64(): string;
    setBytes(value: Uint8Array | string): Value;

    hasValueMap(): boolean;
    clearValueMap(): void;
    getValueMap(): ValueMap | undefined;
    setValueMap(value?: ValueMap): Value;

    hasValueList(): boolean;
    clearValueList(): void;
    getValueList(): ValueList | undefined;
    setValueList(value?: ValueList): Value;

    getValueCase(): Value.ValueCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Value.AsObject;
    static toObject(includeInstance: boolean, msg: Value): Value.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Value, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Value;
    static deserializeBinaryFromReader(message: Value, reader: jspb.BinaryReader): Value;
}

export namespace Value {
    export type AsObject = {
        string: string,
        bool: boolean,
        pb_int: number,
        pb_long: number,
        pb_float: number,
        pb_double: number,
        bytes: Uint8Array | string,
        valueMap?: ValueMap.AsObject,
        valueList?: ValueList.AsObject,
    }

    export enum ValueCase {
        VALUE_NOT_SET = 0,
        STRING = 2,
        BOOL = 3,
        INT = 4,
        LONG = 5,
        FLOAT = 6,
        DOUBLE = 7,
        BYTES = 8,
        VALUE_MAP = 9,
        VALUE_LIST = 10,
    }

}

export class ValueMap extends jspb.Message { 

    getMapMap(): jspb.Map<string, Value>;
    clearMapMap(): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ValueMap.AsObject;
    static toObject(includeInstance: boolean, msg: ValueMap): ValueMap.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ValueMap, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ValueMap;
    static deserializeBinaryFromReader(message: ValueMap, reader: jspb.BinaryReader): ValueMap;
}

export namespace ValueMap {
    export type AsObject = {

        mapMap: Array<[string, Value.AsObject]>,
    }
}

export class ValueList extends jspb.Message { 
    clearElementsList(): void;
    getElementsList(): Array<Value>;
    setElementsList(value: Array<Value>): ValueList;
    addElements(value?: Value, index?: number): Value;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ValueList.AsObject;
    static toObject(includeInstance: boolean, msg: ValueList): ValueList.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ValueList, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ValueList;
    static deserializeBinaryFromReader(message: ValueList, reader: jspb.BinaryReader): ValueList;
}

export namespace ValueList {
    export type AsObject = {
        elementsList: Array<Value.AsObject>,
    }
}

export class JsonPatchOp extends jspb.Message { 
    getOp(): Op;
    setOp(value: Op): JsonPatchOp;
    getPath(): string;
    setPath(value: string): JsonPatchOp;

    hasValue(): boolean;
    clearValue(): void;
    getValue(): Value | undefined;
    setValue(value?: Value): JsonPatchOp;

    hasFromPath(): boolean;
    clearFromPath(): void;
    getFromPath(): google_protobuf_wrappers_pb.StringValue | undefined;
    setFromPath(value?: google_protobuf_wrappers_pb.StringValue): JsonPatchOp;

    hasFromValue(): boolean;
    clearFromValue(): void;
    getFromValue(): Value | undefined;
    setFromValue(value?: Value): JsonPatchOp;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): JsonPatchOp.AsObject;
    static toObject(includeInstance: boolean, msg: JsonPatchOp): JsonPatchOp.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: JsonPatchOp, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): JsonPatchOp;
    static deserializeBinaryFromReader(message: JsonPatchOp, reader: jspb.BinaryReader): JsonPatchOp;
}

export namespace JsonPatchOp {
    export type AsObject = {
        op: Op,
        path: string,
        value?: Value.AsObject,
        fromPath?: google_protobuf_wrappers_pb.StringValue.AsObject,
        fromValue?: Value.AsObject,
    }
}

export class PatchEvent extends jspb.Message { 

    hasIdentity(): boolean;
    clearIdentity(): void;
    getIdentity(): byhiras_identity_identity_pb.Identity | undefined;
    setIdentity(value?: byhiras_identity_identity_pb.Identity): PatchEvent;

    hasTargetidentity(): boolean;
    clearTargetidentity(): void;
    getTargetidentity(): byhiras_identity_identity_pb.Identity | undefined;
    setTargetidentity(value?: byhiras_identity_identity_pb.Identity): PatchEvent;
    clearPatchOpsList(): void;
    getPatchOpsList(): Array<JsonPatchOp>;
    setPatchOpsList(value: Array<JsonPatchOp>): PatchEvent;
    addPatchOps(value?: JsonPatchOp, index?: number): JsonPatchOp;
    getCommitterUsername(): string;
    setCommitterUsername(value: string): PatchEvent;
    getDateTime(): number;
    setDateTime(value: number): PatchEvent;
    getDateTimeOld(): number;
    setDateTimeOld(value: number): PatchEvent;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PatchEvent.AsObject;
    static toObject(includeInstance: boolean, msg: PatchEvent): PatchEvent.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PatchEvent, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PatchEvent;
    static deserializeBinaryFromReader(message: PatchEvent, reader: jspb.BinaryReader): PatchEvent;
}

export namespace PatchEvent {
    export type AsObject = {
        identity?: byhiras_identity_identity_pb.Identity.AsObject,
        targetidentity?: byhiras_identity_identity_pb.Identity.AsObject,
        patchOpsList: Array<JsonPatchOp.AsObject>,
        committerUsername: string,
        dateTime: number,
        dateTimeOld: number,
    }
}

export class PatchEventRequest extends jspb.Message { 

    hasTargetidentity(): boolean;
    clearTargetidentity(): void;
    getTargetidentity(): byhiras_identity_identity_pb.Identity | undefined;
    setTargetidentity(value?: byhiras_identity_identity_pb.Identity): PatchEventRequest;

    hasTargetentitytype(): boolean;
    clearTargetentitytype(): void;
    getTargetentitytype(): byhiras_identity_identity_pb.EntityType;
    setTargetentitytype(value: byhiras_identity_identity_pb.EntityType): PatchEventRequest;
    clearPatchOpsList(): void;
    getPatchOpsList(): Array<JsonPatchOp>;
    setPatchOpsList(value: Array<JsonPatchOp>): PatchEventRequest;
    addPatchOps(value?: JsonPatchOp, index?: number): JsonPatchOp;

    getTargetCase(): PatchEventRequest.TargetCase;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PatchEventRequest.AsObject;
    static toObject(includeInstance: boolean, msg: PatchEventRequest): PatchEventRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PatchEventRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PatchEventRequest;
    static deserializeBinaryFromReader(message: PatchEventRequest, reader: jspb.BinaryReader): PatchEventRequest;
}

export namespace PatchEventRequest {
    export type AsObject = {
        targetidentity?: byhiras_identity_identity_pb.Identity.AsObject,
        targetentitytype: byhiras_identity_identity_pb.EntityType,
        patchOpsList: Array<JsonPatchOp.AsObject>,
    }

    export enum TargetCase {
        TARGET_NOT_SET = 0,
        TARGETIDENTITY = 1,
        TARGETENTITYTYPE = 2,
    }

}

export class JsonPatchRequest extends jspb.Message { 

    hasPatch(): boolean;
    clearPatch(): void;
    getPatch(): PatchEvent | undefined;
    setPatch(value?: PatchEvent): JsonPatchRequest;
    clearPatchesList(): void;
    getPatchesList(): Array<PatchEventRequest>;
    setPatchesList(value: Array<PatchEventRequest>): JsonPatchRequest;
    addPatches(value?: PatchEventRequest, index?: number): PatchEventRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): JsonPatchRequest.AsObject;
    static toObject(includeInstance: boolean, msg: JsonPatchRequest): JsonPatchRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: JsonPatchRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): JsonPatchRequest;
    static deserializeBinaryFromReader(message: JsonPatchRequest, reader: jspb.BinaryReader): JsonPatchRequest;
}

export namespace JsonPatchRequest {
    export type AsObject = {
        patch?: PatchEvent.AsObject,
        patchesList: Array<PatchEventRequest.AsObject>,
    }
}

export enum Op {
    ADD = 0,
    REMOVE = 1,
    REPLACE = 2,
    COPY = 3,
    MOVE = 4,
    TEST = 5,
    SET_ADD = 6,
    SET_REMOVE = 7,
}
