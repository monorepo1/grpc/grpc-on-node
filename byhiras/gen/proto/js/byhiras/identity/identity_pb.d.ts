// package: byhiras.identity
// file: byhiras/identity/identity.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Identity extends jspb.Message { 
    getEntityType(): EntityType;
    setEntityType(value: EntityType): Identity;
    getId(): number;
    setId(value: number): Identity;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Identity.AsObject;
    static toObject(includeInstance: boolean, msg: Identity): Identity.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Identity, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Identity;
    static deserializeBinaryFromReader(message: Identity, reader: jspb.BinaryReader): Identity;
}

export namespace Identity {
    export type AsObject = {
        entityType: EntityType,
        id: number,
    }
}

export class OptionalIdentity extends jspb.Message { 

    hasOptional(): boolean;
    clearOptional(): void;
    getOptional(): Identity | undefined;
    setOptional(value?: Identity): OptionalIdentity;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): OptionalIdentity.AsObject;
    static toObject(includeInstance: boolean, msg: OptionalIdentity): OptionalIdentity.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: OptionalIdentity, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): OptionalIdentity;
    static deserializeBinaryFromReader(message: OptionalIdentity, reader: jspb.BinaryReader): OptionalIdentity;
}

export namespace OptionalIdentity {
    export type AsObject = {
        optional?: Identity.AsObject,
    }
}

export class ListIdentity extends jspb.Message { 
    clearIdentitiesList(): void;
    getIdentitiesList(): Array<Identity>;
    setIdentitiesList(value: Array<Identity>): ListIdentity;
    addIdentities(value?: Identity, index?: number): Identity;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ListIdentity.AsObject;
    static toObject(includeInstance: boolean, msg: ListIdentity): ListIdentity.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ListIdentity, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ListIdentity;
    static deserializeBinaryFromReader(message: ListIdentity, reader: jspb.BinaryReader): ListIdentity;
}

export namespace ListIdentity {
    export type AsObject = {
        identitiesList: Array<Identity.AsObject>,
    }
}

export enum EntityType {
    NULL = 0,
    LEGAL_ENTITY = 1,
    LEGAL_ENTITY_RELATIONSHIP = 2,
    FILE_EXPECTATION = 3,
    PERMISSION_ROLE = 4,
    ACCESS_CONTROL_ENTRY = 5,
    CLASSIFICATION = 6,
    CLASSIFICATION_OF_OBJECT = 7,
    INSTRUMENT = 8,
    PATCH_EVENT = 9,
    COST_REPORT = 10,
    PERMISSION_ROLE_ASSIGNMENT = 11,
    APPLICATION = 12,
    FUND_DETAILS = 13,
    RELATIONSHIP = 14,
    IDENTIFICATION_DOMAIN = 15,
    PERSON = 16,
    PARTICIPANT_DATA_CONFIGURATION = 17,
    DATA_SOURCE = 18,
    PARSER = 19,
    RECEIVED_FILE = 20,
    MATCHED_FILE = 21,
}
